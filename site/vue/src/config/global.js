module.exports.lhost = "http://127.0.0.1:8080"
module.exports.graphdb = "http://127.0.0.1:7200"
module.exports.flaskSparql = "http://127.0.0.1:8080/sparql"
module.exports.flaskDownload = "http://127.0.0.1:8080/api/sparql"